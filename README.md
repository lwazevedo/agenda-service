### Pós clonar ! ###

* npm install
* npm start

### Caso tenha necessidade instale o sailsJS. Do contrário não instale !###
* sudo npm i -g sails

### Rotas: seguem padrão rest - Ex:!###

*Obs: Todas as rotas são baseadas nos campos da agenda de compromisso.

```
* http://localhost:1337/compromissos?page=1&limit=1
* {
  "data": [
    {
      "titulo": "NodeJS tttt",
      "dataIni": "02/01/2017",
      "horaIni": "01:05",
      "dataTermino": "03/01/2017",
      "horaTermino": "16:45",
      "createdAt": "2017-01-13T14:11:38.139Z",
      "updatedAt": "2017-01-13T14:36:18.142Z",
      "id": 31
    }
  ],
  "criteria": {},
  "limit": 1,
  "start": 0,
  "end": 1,
  "page": 1,
  "count": 2
}
```
```
* http://localhost:1337/compromissos 
*{
  "data": [
    {
      "titulo": "NodeJS tttt",
      "dataIni": "02/01/2017",
      "horaIni": "01:05",
      "dataTermino": "03/01/2017",
      "horaTermino": "16:45",
      "createdAt": "2017-01-13T14:11:38.139Z",
      "updatedAt": "2017-01-13T14:36:18.142Z",
      "id": 31
    },
    {
      "titulo": "teste",
      "dataIni": "02/01/2017",
      "horaIni": "01:05",
      "dataTermino": "03/01/2017",
      "horaTermino": "16:45",
      "createdAt": "2017-01-13T14:35:59.970Z",
      "updatedAt": "2017-01-13T14:35:59.970Z",
      "id": 32
    }
  ],
  "criteria": {},
  "limit": 30,
  "start": 0,
  "end": 30,
  "page": 0,
  "count": 2
}
```
```
* http://localhost:1337/compromissos/32 
*{
  "titulo": "teste",
  "dataIni": "02/01/2017",
  "horaIni": "01:05",
  "dataTermino": "03/01/2017",
  "horaTermino": "16:45",
  "createdAt": "2017-01-13T14:35:59.970Z",
  "updatedAt": "2017-01-13T14:35:59.970Z",
  "id": 32
}
```
```
* http://localhost:1337/compromissos/31?titulo="NodeJS"
*{
  "titulo": "NodeJS tttt",
  "dataIni": "02/01/2017",
  "horaIni": "01:05",
  "dataTermino": "03/01/2017",
  "horaTermino": "16:45",
  "createdAt": "2017-01-13T14:11:38.139Z",
  "updatedAt": "2017-01-13T14:36:18.142Z",
  "id": 31
}
```