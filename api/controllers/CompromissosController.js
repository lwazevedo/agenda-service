/**
 * CompromissosController
 *
 * @description :: Server-side logic for managing compromissos
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

 const _ = require('lodash');
 const actionUtil = require('sails/lib/hooks/blueprints/actionUtil');
 const takeAlias = _.partial(_.map, _, item => item.alias);
 const populateAlias = (model, alias) => model.populate(alias);

module.exports = {
  find(req, res) {
    _.set(req.options, 'criteria.blacklist', ['fields', 'populate', 'limit', 'skip', 'page', 'sort']);

    const fields = req.param('fields') ? req.param('fields').replace(/ /g, '').split(',') : [];
        // const sum = req.param('sum');
        const populate = req.param('populate') ? req.param('populate').replace(/ /g, '').split(',') : [];
        const Model = actionUtil.parseModel(req);
        const limit = actionUtil.parseLimit(req);
        const skip = (req.param('page') - 1) * limit || actionUtil.parseSkip(req);
        const sort = actionUtil.parseSort(req);

        Compromissos.find().exec(function(err, compromissos) {

            const where = actionUtil.parseCriteria(req);

            const query = Model.find(null, fields.length > 0 ? {select: fields} : null).where(where).limit(limit).skip(skip).sort(sort);

            let total;
            Compromissos.count(where).exec(function cb(exception, count){                
                exception ? total = 0 : total = count;
            });

            
            const findQuery = _.reduce(_.intersection(populate, takeAlias(Model.associations)), populateAlias, query);
            let array = [];
            findQuery
            .then(function(records) {
                              
                return [{
                        data : records,
                        criteria: where,
                        limit: limit,
                        start: skip,
                        end: skip + limit,
                        page: ((req.param('page') * 1) || actionUtil.parseSkip(req)),
                        count: total
                    }];
            })
            .spread(res.ok)
            .catch(res.negotiate);
        });
    }	
};

